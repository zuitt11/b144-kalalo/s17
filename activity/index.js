let students = []
console.log(students);

function addStudent(student){
		return students.push(student)
	}
	addStudent('marie');
	addStudent('carlos');
	addStudent('mandy');

	console.log(`${students} was added to the list`);

function countStudents(){};
console.log(`There are a total of ${students.length} students enrolled`);


function printStudents(){
	students.sort();
		students.forEach(function(name){
				console.log(name)
		});
};
printStudents()

function findStudent(keyword){
	let match = students.filter(function(name){
			return name.toLowerCase().includes(keyword.toLowerCase())
	})	

	if (match.length == 1) {
		console.log(`${match} is an enrollee.`)
	}else if (match.length > 1) {
		console.log (`${match} are enrollees.`)
	} else {
		console.log (`${match} is not enrollee`)
	}
}
findStudent ('Mar')
findStudent ('aR')
findStudent ('All')


// stretch goals
let addSection = students.map(function(student){
	return `${student} - Section A`
})

console.log(addSection)


let removeStudent = students.slice(0, 3);
console.log(removeStudent);
console.log(students);

let studentIndex = removeStudent.indexOf ('marie b.')
console.log(studentIndex);

let studentSplice = removeStudent.splice(1, 2)
console.log(studentSplice);
console.log(removeStudent);

console.log(`${studentSplice} were removed from the students' list.`)