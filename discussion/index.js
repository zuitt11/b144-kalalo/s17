// ARRAY MANIPULATION

console.log ('Array Manipulation')

let array = [1,2,3];
console.log(array);

let arr = new Array(1,2,3);
console.log(arr);

console.log(array[0]);

// adding array element
let count = ['one', 'two', 'three', 'four'];

console.log(count[3]);
	// assignment operator
	count [4] = 'five'
	console.log(count[4]);

	// push method (add element at the end of the array)
	console.log(count.push('element'));
	console.log(count)

	function pushMethod(element){
		return count.push(element)
	}
	pushMethod('six');
	pushMethod('seven');
	pushMethod('eight');

	console.log(count);

	// pop method array.pop(); removes the last element of the array, can't delete anything else
	count.pop();
	console.log(count)

	function popMethod(){
		return count.pop()
	}

// adding elemets at the beginning of an array

// unshift method array.unshift()

count.unshift('hugot')
console.log(count)

function unshiftMethod(element){
	return count.unshift(element)
}
unshiftMethod('zero')
console.log(count)

// shift method - removing the first element

count.shift()
console.log(count)

function shiftMethod(){
	return count.shift()
}
console.log(count)



// sort method - 
let num = [15, 32, 61, 130, 13, 34];
num.sort()
console.log(num)
// Asceding order
num.sort(
	function(a, b){
		return a - b
	}
)
console.log(num)

// descending
num.sort(
	function(a, b){
		return b - a
	}
)
console.log(num)

// reverse method array.reverse()

num.reverse()
console.log(num)

// splice & slice

// splice array.splice(); returns an array of omitted elements; directly manipulates the original array;
	//1st parameter - index where to start omitting element
	//2nd parameter - number of elements to be omitted startin from the 1st parameter
	//3rd parameter - elements to be added in place of the omitted elements
console.log(count)
/*
let newSplice = count.splice(1);
console.log(newSplice)*/

console.log(count)
/*let newSplice = count.splice(1, 2);
console.log(newSplice);
*/

let newSplice = count.splice(1, 2, 'a1', 'b2', 'c3');
console.log(newSplice);

console.log(count);


// Slice Method array.slice(start, end)
	// 1st parameter - index where to start omitting elements
	// 2nd parameter - # of elements to be omitted (index - 1)
	// 

console.log(count)
/*
let newSlice = count.slice(1);
console.log(newSlice)
console.log(count)*/

let newSlice = count.slice(4, 8);
console.log(newSlice);
console.log(count);

// concat method array.concat()
// 	used to merge two or more arrays

console.log(count);
console.log(num);
let animals = ['bird', 'cat', 'dog', 'fish'];

let newConcat = count.concat(num, animals)
console.log(newConcat)


// join method array.join
	// 
let meal = ['rice', 'steak', 'juice']

let newJoin = meal.join();
console.log(newJoin)

 newJoin = meal.join("");
console.log(newJoin)

 newJoin = meal.join(" ");
console.log(newJoin)

 newJoin = meal.join("-");
console.log(newJoin)
// toString method
console.log(num)
console.log(typeof num[3])

let newString=num.toString();
console.log(typeof newString)

// Accessors
let countries = ['US', 'PH', 'CAN', 'SG', 'HK', 'PH', 'NZ']

// indexOf array.indexOf()
	// finds the location of an element where it is FIRST FOUND;
let index = countries.indexOf('PH')
console.log(index)

index = countries.indexOf('AU') //=1 if the element is not seen
console.log(index)

// lastindexOf()

let lastIndexOf = countries.lastIndexOf('PH');
console.log(lastIndexOf);

// Iterators

//forEach array.forEach(cb())
let days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
	
	days.forEach(
		function(element){
			console.log (element)

		}
	)
//map array.map()
	//returns a copy of the original array but can be manipulated
let mapDays = days.map(function(day){
	return `${day} is the day of the week`
})

console.log(mapDays)


let days2 = days.slice(0, 7);
console.log(days2)
	days2.forEach(
		function (element){
			console.log(element)
		}
	)



//filter array.filter(cb())
console.log(num)

let newFilter= num.filter(function(num){
		return num<50
})

console.log(newFilter)

//includes array.includes

// let newIcludes = animals.includes('')
// console.log(newIcludes)


// checking if the value is present in the array

function wat(animal){
	if (animal.includes(animal)===true) {
			return `${animal}  is existing`
	}else {
		return `${animal} is not found`
	}
}

console.log (wat('cat'))
console.log (wat('aw'))
console.log (wat('fish'))


//everybody every(cb())
	// boolean
		// true only if all elements passed the condition

let newEvery = num.every(function(num){
	return (num > 50)
})
console.log (newEvery)


//some(cb())
	//boolean

let newSome = num.some(function(num){
	return (num > 50)
})
console.log (newSome)


//reduce(cb(<previous>, <current>)) 
let newReduce = num.reduce(function(a, b){
	return a+b
})
console.log(newReduce)

let ave = newReduce/num.length
//toFixed(# of decimals) - returns string

console.log(parseInt(ave));